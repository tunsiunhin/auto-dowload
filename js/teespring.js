$(document).ready(function(){
	var keyword;
	var max,page=0;
	var front=0,back=0;
	$('.btn-search').click(function()
	{
		keyword = $('.text-search').val();
		max = parseInt($('.input-max').val());
		page = ($('.input-start').val()) ? parseInt($('.input-start').val()) : 0;
		let html = '';
		if(!keyword || !max)
		{
			alert('Not Keyword or Max');
			return;
		}
		$(".data-res").html(html);
		keyword = keyword.trim();
		load_camp();
	});
	function load_camp()
	{
		let url = 'https://xnf09ccdo4-dsn.algolia.net/1/indexes/root_search_index_production/query?x-algolia-agent=Algolia for vanilla JavaScript 3.26.0&x-algolia-application-id=XNF09CCDO4&x-algolia-api-key=f0702b1d5af53e24923c31d6bfdb5164';
		let data = '{"params":"query='+keyword+'&page='+page+'&hitsPerPage='+max+'&attributesToRetrieve=%5B%22name%22%2C%22url%22%2C%22primary_pic_url%22%2C%22endcost%22%2C%22enddate%22%2C%22products%22%2C%22region%22%5D&facets=%5B%22product_classification_ids%22%2C%22product_brand_ids%22%2C%22product_brand_and_classification_ids%22%5D&filters=product_classification_ids%3A%22hoodie%22"}';
		$.ajax({
			url:url,
			dataType:'json',
			method:'post',
			data:data,
			beforeSend:function()
			{

			},
			success:function(res)
			{
				let html;
				$('.nbHits').text('nbHits : '+res.nbHits);			
				if(res.hits)
				{
					$.each(res.hits, function( key, value ) {
							
						$.each(value.products,function(k,v){
							if(v.classification == 't_shirt' && v.brand == 'hanes'){
									console.log(v);
									let link = v.image_url;
									link = (link.indexOf('://') === -1) ? 'https://' + link : link;

									let max_size = link.replace("560/560", "2000/2000");
									let mini_size = link.replace("560/560", "360/360");
									html += '<div class="col-md-3 t-shirt">';
									html += '<a  class="link-img"  href="javascript:0;"  data-picture="'+max_size+'" data-name="'+value.name+'"  >';
									html +=	'<img src="'+mini_size+'" class="img-responsive">';
									html += '</a>';
									html += '</div>';
							}
						

							});
	
						});
	
					$(".data-res").append(html);
					page = page + 1;
					$('#page-tee').val(page);
					
				}
			},
			error:function()
			{
				alert('Ô có lỗi rồi. Liện Hệ Em Tuấn.');
			}
		})
	}
	$(document).on('click','.t-shirt',function(){
		$(this).css('background','red');
	});
	function toDataUrl(url, callback) {
		var xhr = new XMLHttpRequest();
		xhr.onload = function() {
			var reader = new FileReader();
			reader.onloadend = function() {
				callback(reader.result);
			}
			reader.readAsDataURL(xhr.response);
		};
		xhr.open('GET', url);
		xhr.responseType = 'blob';
		xhr.send();
	}
	$(document).on('click','.link-img',function(e){
		e.preventDefault();
		let name = $(this).attr('data-name');
		let url  = $(this).attr('data-picture');
		
        toDataUrl(url, function(myBase64) {
			var link = document.createElement('a');
                link.href = myBase64;  // use realtive url 
                link.download = name;
                document.body.appendChild(link);
                link.click()
		});
	}); 
	/*$('.link-img').click(function(e) {
		
    });*/
});


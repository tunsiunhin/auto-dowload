$(document).ready(function(){
	var keyword;
	var max,page=0;
	var front=0,back=0;
	var sort = 'price';
	$('.btn-search').click(function(){

		keyword = $('.text-search').val();
		max = parseInt($('.input-max').val());
		page = ($('.input-start').val()) ? parseInt($('.input-start').val()) : 0;
		sort= $('#sel1').val();
		let html = '';
		if($('.shirt-front').is(':checked'))
			front = 1;
		else
			front = 0;
		if($('.shirt-back').is(':checked'))
			back = 1;
		else
			back = 0;
		if(!keyword || !max)
		{
			alert('Not Keyword or Max');
			return;
		}
		if(front == 0 && back == 0)
		{
			alert('Chọn Áo Trước Hoặc Sau');
			return;
		}

		$(".data-res").html(html);
		keyword = keyword.trim();
		load_camp();
	});
	function load_camp()
	{
		let url = 'https://teechip.com/rest/retail-products/groups/587d0d41cee36fd012c64a69/search?product=shirt&color=black&department=men&query='+keyword+'&page='+page+'&limit='+max+'&sort='+sort;
		$.ajax({
			url:url,
			dataType:'json',
			beforeSend:function()
			{

			},
			success:function(res)
			{
				let html;
				if(res.retailProducts)
				{
					$.each(res.retailProducts, function( key, value ) {
						let link = value.images;
						console.log(front);
						if(front == 1)
						{
							html += '<div class="col-md-3 t-shirt">';
							html += '<a  class="link-img" href="'+link[0].prefix+'/regular.jpg" download="'+value.names.design+'.jpg">';
							html +=	'<img src="'+link[0].prefix+'/regular.jpg" class="img-responsive">';
							html += '</a>';
							html += '<h5 style="text-align:center">'+value.createdAt+'</h5>';
							html += '</div>';
						}
						if(back == 1)
						{
							html += '<div class="col-md-3 t-shirt">';
							html += '<a  class="link-img" href="'+link[1].prefix+'/regular.jpg" download="'+value.names.design+'.jpg">';
							html +=	'<img src="'+link[1].prefix+'/regular.jpg" class="img-responsive">';
							html += '</a>';
							html += '<h5 style="text-align:center">'+value.createdAt+'</h5>';
							html += '</div>';
						}
						
						});
	
					$(".data-res").append(html);
					page = page + 1;
					$('#page-tee').val(page);
					
				}
			},
			error:function()
			{
				alert('Ô có lỗi rồi. Liện Hệ Em Tuấn.');
			}
		})
	}
	$(document).on('click','.t-shirt',function(){
		$(this).css('background','red');
	});
});
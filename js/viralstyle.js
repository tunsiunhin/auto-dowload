$(document).ready(function(){
	var img = [];
	var limit = 500;
	var start = 0;
	var text = '';
	var key ;
	$('.input-key').val(localStorage.getItem("key"));
	$('.btn-search').click(function(){
		let html = '';
		text = $('.text-search').val();
		key  = localStorage.getItem("key");
		limit = $('.input-max').val() ? parseInt($('.input-max').val()) : 500;
		start = $('#start-vir').val() ? parseInt($('#start-vir').val()) : 0;
		if(!text)
		{
			alert('Not Keyword');
			return;
		}
		$(".data-res").html(html);
		text = text.trim();
		load_camping(start,text);
		
	});
	$('.btn-load-more').click(function(){
		if(!text)
		{
			alert('Not keyword');
			return;
		}
		load_camping(start,text);
	});
	$('.btn-add-key').click(function(){
		 key = $('.input-key').val();
		 localStorage.setItem("key", key);
		 alert('Success');
	});
	$(document).on('click','.t-shirt',function(){
		$(this).css('background','red');
	});
	function load_camping(offset,keyword)
	{
		let url  = 'https://viralstyle.com/api/v2/market/search?from='+offset+'&per_page='+limit+'&term='+keyword+'&type=1';
		if(!key)
		{
			load_key();
			return;
		}
		$.ajax({
			url:url,
			method:'get',
			headers: {
				'authorization':'Bearer '+localStorage.getItem("key")+'',
			 },
			beforesend:function()
			{
				$('.btn-status').text('Loading..');
			},
			statusCode: {
		      401:function() { load_key(); },
		    },
			success:function(res)
			{
				let html;
				if(res.data.results)
				{
					$.each(res.data.results, function( key, value ) {
						let link = value.front_image;
						link = link.replace("medium", "large");
						link = link.replace("_front","_front_large");
						html += '<div class="col-md-3 t-shirt">';
						html += '<a  class="link-img" href="'+link+'" download="'+value.name+'.jpg">';
						html +=	'<img src="'+link+'" class="img-responsive">';
						html += '</a>';
						html += '</div>';
						});
					$(".data-res").append(html);
					start = start + limit;
					$('#start-vir').val(start);
					
				}
			},error:function(res)
			{
				console.log(res);
			}
		});

	}
	function load_key()
	{
		let url = 'https://viralstyle.com/api/v2/token';
		$.ajax({
			url:url,
			method:'post',
			dataType:'json',
			data:{'grant_type':'client_credentials','client_id':'frontend','client_secret':'frontend','scope':'public'},
			success:function(res)
			{
				localStorage.setItem("key", res.access_token);
				alert('Key Da duoc load .');
			}
		});
	}

});